//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

package seamless.linklayer.ethernet;

@namespace();

import nesting.ieee8021cb.block.BlockDuplication;
import nesting.ieee8021cb.block.VLANEncapSW;
import nesting.ieee8021q.queue.Queuing;
import nesting.linklayer.ethernet.VLANEncap;
import nesting.linklayer.framePreemption.FrameForward;
import nesting.ieee8021cb.block.DuplicateChecking;
import nesting.linklayer.framePreemption.EtherMACFullDuplexPreemptable;
import inet.linklayer.contract.IEthernetInterface;
import inet.linklayer.ethernet.IEtherEncap;
import inet.networklayer.common.InterfaceEntry;

//
// This module implements an ethernet interface that supports frame preemption.
//
module VlanEthernetInterfaceSwitchPreemptable extends InterfaceEntry like IEthernetInterface
{
    parameters:
        @class(inet::InterfaceEntry);
        string interfaceTableModule;
        string address = default("auto"); // MAC address
        string fcsMode @enum("declared","computed") = default("declared");
        @display("i=block/ifcard;bgl=2");
        *.interfaceTableModule = default(absPath(interfaceTableModule));
        *.fcsMode = fcsMode;
        bool framePreemptionDisabled = default(true);
    gates:
        input upperLayerIn;
        output upperLayerOut;
        inout phys @labels(EtherFrame);
    submodules:
        // mac supporting frame preemption
        mac: EtherMACFullDuplexPreemptable {
            @display("p=154,478");
        }
        queuing: Queuing {
            parameters:
                @display("p=229,61;q=l2queue");
        }
       vlanEncapE: DuplicateChecking {
            @display("p=90,395");
        }
        etherEncapE: <default("EtherEncap")> like IEtherEncap {
            parameters:
                @display("p=90,245");
        }
        vlanEncapP: DuplicateChecking {
            @display("p=229,395");
        }
        etherEncapP: <default("EtherEncap")> like IEtherEncap {
            parameters:
                @display("p=229,245");
        }
        frameForward: FrameForward {
            @display("p=90,61");
        }

        blockDuplication: BlockDuplication {
            @display("p=90,160");
        }
//        duplicateChecking: DuplicateChecking {
  //          @display("p=136,316");
    //    }
      //  duplicateChecking1: DuplicateChecking {
        //    @display("p=268,316");
       // }
    connections:
        upperLayerIn --> queuing.in;
        frameForward.upperLayerOut --> upperLayerOut;

        blockDuplication.upperLayerOut --> frameForward.lowerLayerInE;
 //       etherEncapE.upperLayerOut --> frameForward.lowerLayerInE;
 //       etherEncapP.upperLayerOut --> frameForward.lowerLayerInP;

        etherEncapE.upperLayerOut --> blockDuplication.lowerLayerInE;
        etherEncapP.upperLayerOut --> blockDuplication.lowerLayerInP;

        queuing.eOut --> etherEncapE.upperLayerIn;
        queuing.pOut --> etherEncapP.upperLayerIn;

        vlanEncapE.lowerLayerOut --> mac.upperLayerIn;
        mac.upperLayerOut --> vlanEncapE.lowerLayerIn;

        vlanEncapP.lowerLayerOut --> mac.upperLayerPreemptableIn;
        mac.upperLayerPreemptableOut --> vlanEncapP.lowerLayerIn;

  //      etherEncapE.lowerLayerOut --> vlanEncapE.upperLayerIn;
  //      vlanEncapE.upperLayerOut --> etherEncapE.lowerLayerIn;

  //      etherEncapP.lowerLayerOut --> vlanEncapP.upperLayerIn;
  //      vlanEncapP.upperLayerOut --> etherEncapP.lowerLayerIn;

        etherEncapE.lowerLayerOut --> vlanEncapE.upperLayerIn;
        vlanEncapE.upperLayerOut --> etherEncapE.lowerLayerIn;

        etherEncapP.lowerLayerOut --> vlanEncapP.upperLayerIn;
        vlanEncapP.upperLayerOut --> etherEncapP.lowerLayerIn;

  //      etherEncapE.lowerLayerOut --> duplicateChecking.upperLayerIn;
  //      duplicateChecking.upperLayerOut --> etherEncapE.lowerLayerIn;

  //      etherEncapP.lowerLayerOut --> duplicateChecking1.upperLayerIn;
  //      duplicateChecking1.upperLayerOut --> etherEncapP.lowerLayerIn;

        mac.phys <--> { @display("m=s"); } <--> phys;
}
