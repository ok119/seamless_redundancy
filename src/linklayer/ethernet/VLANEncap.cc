//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "VLANEncap.h"
#define COMPILETIME_LOGLEVEL omnetpp::LOGLEVEL_TRACE

namespace nesting {

Define_Module(VLANEncap);

void VLANEncap::initialize(int stage) {
    if (stage == inet::INITSTAGE_LOCAL) {
        // Signals
        encapPkSignal = registerSignal("encapPk");
        decapPkSignal = registerSignal("decapPk");

        verbose = par("verbose");
        tagUntaggedFrames = par("tagUntaggedFrames");
        pvid = par("pvid");
        pseq = par("pseq");

        totalFromHigherLayer = 0;
        WATCH(totalFromHigherLayer);

        totalFromLowerLayer = 0;
        WATCH(totalFromLowerLayer);

        totalEncap = 0;
        WATCH(totalEncap);

        totalDecap = 0;
        WATCH(totalDecap);
    } else if (stage == inet::INITSTAGE_LINK_LAYER) {

    }

}

void VLANEncap::handleMessage(cMessage* msg) {
    if (dynamic_cast<inet::Request *>(msg) != nullptr) {
        send(msg, "lowerLayerOut");
        return;
    }
    inet::Packet* packet = check_and_cast<inet::Packet*>(msg);

    if (packet->arrivedOn("lowerLayerIn")) {
        processPacketFromLowerLevel(packet);
    } else {
        processPacketFromHigherLevel(packet);
    }
}

void VLANEncap::processPacketFromHigherLevel(inet::Packet *packet) {
    EV_INFO << getFullPath() << ": Received " << packet << " from upper layer."
                   << endl;
    pseq++;
    totalFromHigherLayer++;

    packet->trimFront();
   const auto& ethernetMacHeader = packet->removeAtFront<   inet::EthernetMacHeader>();
//    const auto& ethernetMacHeader = packet->removeAtFront<   EthernetcbMacHeader>();
    auto vlanTag = packet->findTag<VLANTagReq>();
    vlanTag->setVID(pseq);

    if (vlanTag) {
        auto vlanHeader = new inet::Ieee8021qHeader();
    //    auto vlanHeader = new Ieee8021cbHeader();
        vlanHeader->setPcp(vlanTag->getPcp());
        vlanHeader->setDe(vlanTag->getDe());
        vlanHeader->setVid(vlanTag->getVID());

        ethernetMacHeader->setCTag(vlanHeader);
//        delete packet->removeTagIfPresent<VLANTagReq>();
        EV_INFO << getFullPath() << ":Encapsulating higher layer packet `"
                       << packet->getName() << "' into VLAN tag" << endl;
//        totalEncap++;
//        emit(encapPkSignal, packet->getTreeId()); // getting tree id, because it doenn't get changed when packet is copied
    }
    auto vlancbTag = packet->findTag<VLANTagReq>();
   // vlancbTag->setVID(pseq);
    if (vlancbTag) {
 //       auto vlanHeader = new inet::Ieee8021qHeader();
        auto vlanHeader = new Ieee8021cbHeader();
        vlanHeader->setPcp(vlancbTag->getPcp());
        vlanHeader->setDe(vlanTag->getDe());
        vlanHeader->setVid(vlanTag->getVID());
        vlanHeader->setRtag(vlanTag->getRtag());
        vlanHeader->setRsv(vlancbTag->getRsv());
        vlanHeader->setSeq(vlanTag->getVID());

        //   vlanHeader->setSeq(pseq);
              auto seq = vlanTag->getSeq();
              auto pcp = vlanHeader->getPcp();

              EV <<"\n"<< ", Seq  vlaue in VLANENCAP " << seq << "  "<<pcp<<"\n";

        ethernetMacHeader->setSTag(vlanHeader);
        delete packet->removeTagIfPresent<VLANTagReq>();
           EV_INFO << getFullPath() << ":Encapsulating higher layer packet `"
                       << packet->getName() << "' into VLAN tag" << endl;
        totalEncap++;
        emit(encapPkSignal, packet->getTreeId()); // getting tree id, because it doenn't get changed when packet is copied
    }
    packet->insertAtFront(ethernetMacHeader);
    auto oldFcs = packet->removeAtBack<inet::EthernetFcs>();
    inet::EtherEncap::addFcs(packet, oldFcs->getFcsMode());


    EV_TRACE << getFullPath() << ": Packet-length is "
                    << packet->getByteLength() << " and Destination is "
                    << packet->getTag<inet::MacAddressReq>()->getDestAddress()
                    << " before sending packet to lower layer" << endl;

    send(packet, "lowerLayerOut");
}

void VLANEncap::processPacketFromLowerLevel(inet::Packet *packet) {
    EV_INFO << getFullPath() << ": Received " << packet << " from lower layer."
                   << endl;
    pseq++;
    totalFromLowerLayer++;

    // Decapsulate packet if it is a VLAN Tag, otherwise just insert default
    // values into the control information
    packet->trimFront();
    const auto& ethernetMacHeader = packet->removeAtFront<inet::EthernetMacHeader>();
//    const auto& ethernetMacHeader = packet->removeAtFront<    EthernetcbMacHeader>();
    auto vlanHeader = ethernetMacHeader->dropCTag();
    vlanHeader = ethernetMacHeader->dropSTag();

    EV_TRACE << getFullPath() << ": Packet-length is "
                    << packet->getByteLength() << ", destination is "
                    << ethernetMacHeader->getDest();

    if (vlanHeader) {
        auto vlanTag = packet->addTagIfAbsent<VLANTagInd>();
        vlanTag->setPcp(vlanHeader->getPcp());
        vlanTag->setDe(vlanHeader->getDe());
        EV_TRACE << ", PCP Value is " << (int) vlanTag->getPcp() << " ";
        short vid = vlanHeader->getVid();
 //       short seq = vlanHeader->getSeq();
        if (vid < kMinValidVID || vid > kMaxValidVID) {
            vid = pvid;
        }
 //       short pseq = vlanHeader->getSeq();

       // vlanTag->setVID(vid);
        vlanTag->setRtag(0xF1C1);
        vlanTag->setRsv(0);
        vlanTag->setSeq(pseq);
        EV_TRACE << getFullPath() << ": Decapsulating packet and `"
                        << "' passing up contained packet `"
                        << packet->getName() << "' to higher layer" << endl;

        totalDecap++;
        emit(decapPkSignal, packet->getTreeId());
    } else {
        auto vlanTag = packet->addTagIfAbsent<VLANTagInd>();
        vlanTag->setPcp(kDefaultPCPValue);
        vlanTag->setDe(kDefaultDEIValue);
   //     vlanTag->setVID(pvid);
        vlanTag->setRtag(0xF1C1);
        vlanTag->setRsv(0);
        vlanTag->setSeq(pseq);

    }
    packet->insertAtFront(ethernetMacHeader);
    auto oldFcs = packet->removeAtBack<inet::EthernetFcs>();
    inet::EtherEncap::addFcs(packet, oldFcs->getFcsMode());

    EV_TRACE << " before sending packet up" << endl;

    // Send packet to upper layer
    send(packet, "upperLayerOut");
  }


void VLANEncap::refreshDisplay() const {
    char buf[80];
    sprintf(buf, "up/decap: %ld/%ld\ndown/encap: %ld/%ld", totalFromLowerLayer,
            totalDecap, totalFromHigherLayer, totalEncap);
    getDisplayString().setTagArg("t", 0, buf);
}
int VLANEncap::getPseq() {
    return pseq;
}
int VLANEncap::getPVID() {
    return pvid;
}

} // namespace nesting
