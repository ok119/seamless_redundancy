//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __STUPRONESTINGPROJECT_FRAMEFORWARD_H_
#define __STUPRONESTINGPROJECT_FRAMEFORWARD_H_

#include <omnetpp.h>

#include "inet/linklayer/common/Ieee802Ctrl.h"
#include "../../ieee8021cb/Ieee8021cbHeader_m.h"
#include "inet/linklayer/common/MacAddressTag_m.h"
//#include "inet/linklayer/ethernet/EtherFrame_m.h"
#include "../../ieee8021cb/Ether/EthercbFrame_m.h"
#include "inet/linklayer/ethernet/EtherEncap.h"
#include "inet/common/packet/Packet.h"
#include "inet/common/packet/Message_m.h"
#include "../common/VLANTag_m.h"
#include "nesting/ieee8021q/Ieee8021q.h"

using namespace omnetpp;
using namespace inet;

namespace nesting {

class FrameForward: public cSimpleModule {
protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);
    short SEQ;
};

} /* namespace nesting */
#endif

