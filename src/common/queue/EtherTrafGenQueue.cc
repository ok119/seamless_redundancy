//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "EtherTrafGenQueue.h"

#include <omnetpp/ccomponent.h>
#include <omnetpp/cexception.h>
#include <omnetpp/clog.h>
#include <omnetpp/cmessage.h>
#include <omnetpp/cnamedobject.h>
#include <omnetpp/cobjectfactory.h>
#include <omnetpp/cpacket.h>
#include <omnetpp/cpar.h>
#include <omnetpp/csimplemodule.h>
#include <omnetpp/cwatch.h>
#include <omnetpp/regmacros.h>
#include <omnetpp/simutil.h>
#include <cstdio>
#include <iostream>
#include <string>
#include<stdio.h>
#include<stdlib.h>
#include "nesting/ieee8021q/clock/IClock.h"

#include "inet/linklayer/common/Ieee802Ctrl.h"
#include <omnetpp/cdataratechannel.h>

namespace nesting {

Define_Module(EtherTrafGenQueue);

void EtherTrafGenQueue::initialize() {
    // Signals
    sentPkSignal = registerSignal("sentPk");

    // Initialize sequence-number for generated packets
    seqNum = 0;
    seqN = 0;

    // NED parameters
    etherType = &par("etherType");
    vlanTagEnabled = &par("vlanTagEnabled");
    pcp = &par("pcp");
    dei = &par("dei");
    vid = &par("vid");
    rtag = &par("rtag");
    rsv = &par("rsv");
    seq = &par("seq");

    packetLength = &par("packetLength");
    randomPacketLengthEnabled = par("randomPacketLengthEnabled");
    const char *destAddress = par("destAddress");
    if (!destMacAddress.tryParse(destAddress)) {
        throw new cRuntimeError("Invalid MAC Address");
    }

    // Statistics
    packetsSent = 0;
    WATCH(packetsSent);

    llcSocket.setOutputGate(gate("out"));

    llcSocket.open(-1, ssap);

}

void EtherTrafGenQueue::handleMessage(cMessage *msg) {
    throw cRuntimeError("cannot handle messages.");
}


Packet* EtherTrafGenQueue::generatePacket() {
    seqNum++;

    char msgname[40];
    //   sprintf(msgname, "pk-%d-%ld", getId(), seqNum);
    sprintf(msgname, "WS_to_BS-%d-%ld", getId(), seqNum);

    // create new packet
    Packet *datapacket = new Packet(msgname, IEEE802CTRL_DATA);
    long len;
    if (randomPacketLengthEnabled) {
        len = cComponent::intuniform(64, 1450, 0);
    } else {
        len = packetLength->intValue();
    }
    const auto& payload = makeShared<ByteCountChunk>(B(len));
    // set creation time
    auto timeTag = payload->addTag<CreationTimeTag>();
    timeTag->setCreationTime(simTime());
 //   timeTag->setCreationTime(simTime().dbl() * 100);

    datapacket->insertAtBack(payload);
    datapacket->removeTagIfPresent<PacketProtocolTag>();
    datapacket->addTagIfAbsent<PacketProtocolTag>()->setProtocol(
            &Protocol::ethernetMac);
    // TODO check if protocol is correct
    auto sapTag = datapacket->addTagIfAbsent<Ieee802SapReq>();
    sapTag->setSsap(ssap);
    sapTag->setDsap(dsap);

    auto macTag = datapacket->addTag<MacAddressReq>();
    macTag->setDestAddress(destMacAddress);

    uint8_t PCP;
    bool de;
    short VID;
    short Rtag;
    short Rsv;
    short Seq;

    // create VLAN control info
    if (vlanTagEnabled->boolValue()) {
        auto ieee8021cb = datapacket->addTag<VLANTagReq>();
        PCP = pcp->intValue();
        de = dei->boolValue();
        VID = vid->intValue();
        Rtag = rtag->intValue();
        Rsv = rsv->intValue();
        Seq = seq->intValue();
        ieee8021cb->setPcp(PCP);
        ieee8021cb->setDe(de);
        ieee8021cb->setVID(VID);
        ieee8021cb->setRtag(0xF1C1);
        ieee8021cb->setRsv(Rsv);
        ieee8021cb->setSeq(Seq);
   }
    return datapacket;
}

void EtherTrafGenQueue::requestPacket() {
    Enter_Method("requestPacket(...)");



    Packet* packet = generatePacket();

    if(par("verbose")) {
        auto macTag = packet->findTag<MacAddressReq>();
        auto ieee8021cbTag = packet->findTag<VLANTagReq>();
        EV_TRACE << getFullPath() << ": Send packet `" << packet->getName() << "' dest=" << macTag->getDestAddress()
        << " length=" << packet->getBitLength() << "B type= empty" << " vlan-tagged=false";
        if(ieee8021cbTag) {
            EV_TRACE << " vlan-tagged=true" << " pcp=" << ieee8021cbTag->getPcp()
            << " dei=" << ieee8021cbTag->getDe() << " vid=" << ieee8021cbTag->getVID()
            << " rtag=" << ieee8021cbTag->getRtag()
            << " rsv=" << ieee8021cbTag->getRsv() << " seq=" << ieee8021cbTag->getSeq();
        }
        EV_TRACE << endl;
    }
    emit(sentPkSignal, packet->getTreeId()); // getting tree id, because it doenn't get changed when packet is copied
    send(packet, "out");
    packetsSent++;
}

int EtherTrafGenQueue::getNumcount() {

  //  EtherTrafGenQueue::generatePacket();
    return seqNum;
}


int EtherTrafGenQueue::getNumPendingRequests() {
    // Requests are always served immediately,
    // therefore no pending requests exist.
    return 0;
}

bool EtherTrafGenQueue::isEmpty() {
    // Queue is never empty
    return false;
}

cMessage* EtherTrafGenQueue::pop() {
    return generatePacket();
}

} // namespace nesting
