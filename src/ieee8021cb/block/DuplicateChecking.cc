/*
 * BlockDuplication.cc
 * this function work as sequence recovery function recovery function
 *  Created on: Mar 31, 2020
 *      Author: omer
 */

#include "DuplicateChecking.h"
#define COMPILETIME_LOGLEVEL omnetpp::LOGLEVEL_TRACE

namespace nesting {

Define_Module(DuplicateChecking);


simsignal_t DuplicateChecking::preemptCurrentFrameSignal =
        registerSignal("preemptCurrentFrameSignal");
simsignal_t DuplicateChecking::transmittedExpressFrameSignal =
        registerSignal("transmittedExpressFrameSignal");
simsignal_t DuplicateChecking::startTransmissionExpressFrameSignal =
        registerSignal("startTransmissionExpressFrameSignal");
simsignal_t DuplicateChecking::transmittedPreemptableFrameSignal =
        registerSignal("transmittedPreemptableFrameSignal");
simsignal_t DuplicateChecking::transmittedPreemptableFramePartSignal =
        registerSignal("transmittedPreemptableFramePartSignal");
simsignal_t DuplicateChecking::transmittedPreemptableNonFinalSignal =
        registerSignal("transmittedPreemptableNonFinalSignal");
simsignal_t DuplicateChecking::transmittedPreemptableFinalSignal =
        registerSignal("transmittedPreemptableFinalSignal");
simsignal_t DuplicateChecking::transmittedPreemptableFullSignal =
        registerSignal("transmittedPreemptableFullSignal");
simsignal_t DuplicateChecking::expressFrameEnqueuedWhileSendingPreemptableSignal =
        registerSignal("expressFrameEnqueuedWhileSendingPreemptableSignal");
simsignal_t DuplicateChecking::eMacDelay = registerSignal(
        "eMacDelay");
simsignal_t DuplicateChecking::pMacDelay = registerSignal(
        "pMacDelay");
simsignal_t DuplicateChecking::receivedExpressFrame =
        registerSignal("receivedExpressFrame");
simsignal_t DuplicateChecking::receivedPreemptableFrameFull =
        registerSignal("receivedPreemptableFrameFull");

void DuplicateChecking::initialize(int stage) {

    if (stage == inet::INITSTAGE_LOCAL) {
        // Signals
        encapPkSignal = registerSignal("encapPk");
        decapPkSignal = registerSignal("decapPk");

        verbose = par("verbose");
        tagUntaggedFrames = par("tagUntaggedFrames");
        pvid = par("pvid");
        pseql = par("pseql");
        pseqh = par("pseqh");

       totalFromHigherLayer = 0;
        WATCH(totalFromHigherLayer);

        totalFromLowerLayer = 0;
        WATCH(totalFromLowerLayer);

        totalEncap = 0;
        WATCH(totalEncap);

        totalDecap = 0;
        WATCH(totalDecap);
    } else if (stage == inet::INITSTAGE_LINK_LAYER) {

    }

}

void DuplicateChecking::handleMessage(cMessage* msg) {
    if (dynamic_cast<inet::Request *>(msg) != nullptr) {
        send(msg, "lowerLayerOut");
        return;
    }
    inet::Packet* packet = check_and_cast<inet::Packet*>(msg);

    if (packet->arrivedOn("lowerLayerIn")) {
        processPacketFromLowerLevel(packet);
    } else {
        processPacketFromHigherLevel(packet);
    }
}

void DuplicateChecking::processPacketFromHigherLevel(inet::Packet *packet) {
    EV_INFO << getFullPath() << ": Received " << packet << " from upper layer."
                   << endl;
    totalFromHigherLayer++;
    short int seq;
    packet->trimFront();
   const auto& ethernetMacHeader = packet->removeAtFront<   inet::EthernetMacHeader>();
    auto vlanTag = packet->findTag<VLANTagReq>();

    if (vlanTag) {
        auto vlanHeader = new inet::Ieee8021qHeader();
        vlanHeader->setPcp(vlanTag->getPcp());
        vlanHeader->setDe(vlanTag->getDe());
        vlanHeader->setVid(vlanTag->getVID());

        ethernetMacHeader->setCTag(vlanHeader);
        EV_INFO << getFullPath() << ":Encapsulating higher layer packet `"
                       << packet->getName() << "' into VLAN tag" << endl;
    }
  auto vlancbTag = packet->findTag<VLANTagReq>();
    if (vlancbTag) {
        auto vlanHeader = new Ieee8021cbHeader();
        vlanHeader->setPcp(vlancbTag->getPcp());
        vlanHeader->setDe(vlanTag->getDe());
        vlanHeader->setVid(vlanTag->getVID());
         vlanHeader->setRtag(vlanTag->getRtag());
        vlanHeader->setRsv(vlancbTag->getRsv());
        vlanHeader->setSeq(vlancbTag->getVID());
              seq = vlanTag->getVID();
              auto pcp = vlanHeader->getPcp();


        ethernetMacHeader->setSTag(vlanHeader);
        delete packet->removeTagIfPresent<VLANTagReq>();
           EV_INFO << getFullPath() << ":Encapsulating higher layer packet `"
                       << packet->getName() << "' into VLAN tag" << endl;
        totalEncap++;
        emit(encapPkSignal, packet->getTreeId()); // getting tree id, because it doenn't get changed when packet is copied
    }
    packet->insertAtFront(ethernetMacHeader);
    auto oldcbFcs = packet->removeAtBack<inet::EthernetFcs>();
    inet::EtherEncap::addFcs(packet, oldcbFcs->getFcsMode());


    EV_TRACE << getFullPath() << ": Packet-length is "
                    << packet->getByteLength() << " and Destination is "
                    << packet->getTag<inet::MacAddressReq>()->getDestAddress()
                    << " before sending packet to lower layer" << endl;

    blockfromupperlayer(packet,seq);
}
void DuplicateChecking::processPacketFromLowerLevel(inet::Packet *packet) {
    EV_INFO << getFullPath() << ": Received " << packet << " from lower layer."
                   << endl;
    totalFromLowerLayer++;
    short int seq;

    // Decapsulate packet if it is a VLAN Tag, otherwise just insert default
    // values into the control information
    packet->trimFront();
    const auto& ethernetMacHeader = packet->removeAtFront<inet::EthernetMacHeader>();
    auto vlanHeader = ethernetMacHeader->dropCTag();
    vlanHeader = ethernetMacHeader->dropSTag();

    EV_TRACE << getFullPath() << ": Packet-length is "
                    << packet->getByteLength() << ", destination is "
                    << ethernetMacHeader->getDest();

    if (vlanHeader) {
        auto vlanTag = packet->addTagIfAbsent<VLANTagInd>();
        vlanTag->setPcp(vlanHeader->getPcp());
        vlanTag->setDe(vlanHeader->getDe());
        EV_TRACE << ", PCP Value is " << (int) vlanTag->getPcp() << " ";
        short vid = vlanHeader->getVid();
        if (vid < kMinValidVID || vid > kMaxValidVID) {
            vid = pvid;
        }

        vlanTag->setVID(vlanHeader->getVid());
        vlanTag->setRtag(0xF1C1);
        vlanTag->setRsv(0);
        vlanTag->setSeq(vlanHeader->getVid());
        EV_TRACE << getFullPath() << ": Decapsulating packet and `"
                        << "' passing up contained packet `"
                        << packet->getName() << "' to higher layer" << endl;
        seq = vlanHeader->getVid();

        totalDecap++;
        emit(decapPkSignal, packet->getTreeId());
    } else {
        auto vlanTag = packet->addTagIfAbsent<VLANTagInd>();
        vlanTag->setPcp(kDefaultPCPValue);
        vlanTag->setDe(kDefaultDEIValue);
        vlanTag->setVID(vlanHeader->getVid());
        vlanTag->setRtag(0xF1C1);
        vlanTag->setRsv(0);
        vlanTag->setSeq(vlaHeader->getVid());  // it is because IEEE 802.1CB header is not seperately used so
                                               // this header used within IEEE 802.1Q

    }

    packet->insertAtFront(ethernetMacHeader);
    auto oldFcs = packet->removeAtBack<inet::EthernetFcs>();
    inet::EtherEncap::addFcs(packet, oldFcs->getFcsMode());
   int snd =1;
    int nodeID = 0;
           nodeID = getId();

           if (nodeID == 1484)
               snd = 2;
           else if (nodeID == 1521)
               snd = 2;
           else if (nodeID == 656)
               snd = 2;

    EV_TRACE << " before sending packet up" << endl;
    // Send packet to upper layer
  if (snd == 1)
    send(packet, "upperLayerOut");
  }


void DuplicateChecking::refreshDisplay() const {
    char buf[80];
    sprintf(buf, "up/decap: %ld/%ld\ndown/encap: %ld/%ld", totalFromLowerLayer,
            totalDecap, totalFromHigherLayer, totalEncap);
    getDisplayString().setTagArg("t", 0, buf);
}

void DuplicateChecking::blockfromupperlayer(inet::Packet *packet, short int seq) {
    pseqh++;
    auto macTag = packet->findTag<MacAddressReq>();
    MACSourceh[pseqh] = macTag->getSrcAddress();

     blockch[pseqh] = seq;
     int chsum = 1;
     int snd = 1;

     while (chsum < pseqh)
     {
         if (blockch[chsum] == blockch[pseqh] && MACSourceh[chsum] == MACSourceh[pseqh])
         {
              snd = 2;
         }
         chsum++;

     }
     if (snd == 1)
         send(packet, "lowerLayerOut");
     else
     {

     }
}
void DuplicateChecking::blockfromlowerlayer(inet::Packet *packet, short int seq) {
    pseql++;
    auto macTag = packet->findTag<MacAddressReq>();
    MACSourcel[pseql] = macTag->getSrcAddress();

     blockch[pseql] = seq;
     int chsum = 1;
     int snd = 1;

     while (chsum < pseql)
     {
         if (blockch[chsum] == blockch[pseql] && MACSourcel[chsum] == MACSourcel[pseql])
         {

                 snd = 2;

         }
         chsum++;

     }

     if (snd == 1)
         send(packet, "upperLayerOut");

}
int DuplicateChecking::getPseql() {
    return pseql;
}
int DuplicateChecking::getPseqh() {
    return pseqh;
}
int DuplicateChecking::getPVID() {
    return pvid;
}

} // namespace nesting
