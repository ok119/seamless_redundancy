/*
 * BlockDuplicationSwitch.cc
 * this file is to created for testing purposes. At the time the there is no
//procedure available to drop a packet atswitch so when manually dropped the packets
//this is just for testing for testing
 *  Created on: Mar 31, 2020
 *      Author: omer
 */

#include "BlockDuplicationSwitch.h"
#define COMPILETIME_LOGLEVEL omnetpp::LOGLEVEL_TRACE

namespace nesting {

Define_Module(BlockDuplicationSwitch);


simsignal_t BlockDuplicationSwitch::preemptCurrentFrameSignal =
        registerSignal("preemptCurrentFrameSignal");
simsignal_t BlockDuplicationSwitch::transmittedExpressFrameSignal =
        registerSignal("transmittedExpressFrameSignal");
simsignal_t BlockDuplicationSwitch::startTransmissionExpressFrameSignal =
        registerSignal("startTransmissionExpressFrameSignal");
simsignal_t BlockDuplicationSwitch::transmittedPreemptableFrameSignal =
        registerSignal("transmittedPreemptableFrameSignal");
simsignal_t BlockDuplicationSwitch::transmittedPreemptableFramePartSignal =
        registerSignal("transmittedPreemptableFramePartSignal");
simsignal_t BlockDuplicationSwitch::transmittedPreemptableNonFinalSignal =
        registerSignal("transmittedPreemptableNonFinalSignal");
simsignal_t BlockDuplicationSwitch::transmittedPreemptableFinalSignal =
        registerSignal("transmittedPreemptableFinalSignal");
simsignal_t BlockDuplicationSwitch::transmittedPreemptableFullSignal =
        registerSignal("transmittedPreemptableFullSignal");
simsignal_t BlockDuplicationSwitch::expressFrameEnqueuedWhileSendingPreemptableSignal =
        registerSignal("expressFrameEnqueuedWhileSendingPreemptableSignal");
simsignal_t BlockDuplicationSwitch::eMacDelay = registerSignal(
        "eMacDelay");
simsignal_t BlockDuplicationSwitch::pMacDelay = registerSignal(
        "pMacDelay");
simsignal_t BlockDuplicationSwitch::receivedExpressFrame =
        registerSignal("receivedExpressFrame");
simsignal_t BlockDuplicationSwitch::receivedPreemptableFrameFull =
        registerSignal("receivedPreemptableFrameFull");

void BlockDuplicationSwitch::initialize(int stage) {

    if (stage == inet::INITSTAGE_LOCAL) {
        // Signals
        encapPkSignal = registerSignal("encapPk");
        decapPkSignal = registerSignal("decapPk");

        verbose = par("verbose");
        tagUntaggedFrames = par("tagUntaggedFrames");
        pvid = par("pvid");
        pseql = par("pseql");
        pseqh = par("pseqh");


        totalFromHigherLayer = 0;
        WATCH(totalFromHigherLayer);

        totalFromLowerLayer = 0;
        WATCH(totalFromLowerLayer);

        totalEncap = 0;
        WATCH(totalEncap);

        totalDecap = 0;
        WATCH(totalDecap);
    } else if (stage == inet::INITSTAGE_LINK_LAYER) {
  //      scheduleIndex = 0;
    //       scheduleXml = par("schedule").xmlValue();

    }

}

void BlockDuplicationSwitch::handleMessage(cMessage* msg) {
    if (dynamic_cast<inet::Request *>(msg) != nullptr) {
        send(msg, "lowerLayerOut");
        return;
    }
    inet::Packet* packet = check_and_cast<inet::Packet*>(msg);

    if (packet->arrivedOn("lowerLayerIn")) {
        processPacketFromLowerLevel(packet);
    } else {
        processPacketFromHigherLevel(packet);
    }
}

void BlockDuplicationSwitch::processPacketFromHigherLevel(inet::Packet *packet) {
    EV_INFO << getFullPath() << ": Received " << packet << " from upper layer."
                   << endl;
    totalFromHigherLayer++;
    short int seq;
    packet->trimFront();
   const auto& ethernetMacHeader = packet->removeAtFront<   inet::EthernetMacHeader>();
    auto vlanTag = packet->findTag<VLANTagReq>();

    if (vlanTag) {
        auto vlanHeader = new inet::Ieee8021qHeader();
        vlanHeader->setPcp(vlanTag->getPcp());
        vlanHeader->setDe(vlanTag->getDe());
        vlanHeader->setVid(vlanTag->getVID());

        ethernetMacHeader->setCTag(vlanHeader);
        EV_INFO << getFullPath() << ":Encapsulating higher layer packet `"
                       << packet->getName() << "' into VLAN tag" << endl;
    }
  auto vlancbTag = packet->findTag<VLANTagReq>();
    if (vlancbTag) {
        auto vlanHeader = new Ieee8021cbHeader();
        vlanHeader->setPcp(vlancbTag->getPcp());
        vlanHeader->setDe(vlanTag->getDe());
        vlanHeader->setVid(vlanTag->getVID());
         vlanHeader->setRtag(vlanTag->getRtag());
        vlanHeader->setRsv(vlancbTag->getRsv());
        vlanHeader->setSeq(vlancbTag->getVID());
              seq = vlanTag->getVID();
              auto pcp = vlanHeader->getPcp();


        ethernetMacHeader->setSTag(vlanHeader);
        delete packet->removeTagIfPresent<VLANTagReq>();
           EV_INFO << getFullPath() << ":Encapsulating higher layer packet `"
                       << packet->getName() << "' into VLAN tag" << endl;
        totalEncap++;
        emit(encapPkSignal, packet->getTreeId()); // getting tree id, because it doenn't get changed when packet is copied
    }
    packet->insertAtFront(ethernetMacHeader);
    auto oldcbFcs = packet->removeAtBack<inet::EthernetFcs>();
    inet::EtherEncap::addFcs(packet, oldcbFcs->getFcsMode());


    EV_TRACE << getFullPath() << ": Packet-length is "
                    << packet->getByteLength() << " and Destination is "
                    << packet->getTag<inet::MacAddressReq>()->getDestAddress()
                    << " before sending packet to lower layer" << endl;

    blockfromupperlayer(packet,seq);
}
void BlockDuplicationSwitch::processPacketFromLowerLevel(inet::Packet *packet) {
    EV_INFO << getFullPath() << ": Received " << packet << " from lower layer."
                   << endl;
    totalFromLowerLayer++;
    short int seq;

    // Decapsulate packet if it is a VLAN Tag, otherwise just insert default
    // values into the control information
    packet->trimFront();
    const auto& ethernetMacHeader = packet->removeAtFront<inet::EthernetMacHeader>();
    auto vlanHeader = ethernetMacHeader->dropCTag();
    vlanHeader = ethernetMacHeader->dropSTag();

    EV_TRACE << getFullPath() << ": Packet-length is "
                    << packet->getByteLength() << ", destination is "
                    << ethernetMacHeader->getDest();

    if (vlanHeader) {
        auto vlanTag = packet->addTagIfAbsent<VLANTagInd>();
        vlanTag->setPcp(vlanHeader->getPcp());
        vlanTag->setDe(vlanHeader->getDe());
        EV_TRACE << ", PCP Value is " << (int) vlanTag->getPcp() << " ";
        short vid = vlanHeader->getVid();
        if (vid < kMinValidVID || vid > kMaxValidVID) {
            vid = pvid;
        }

        vlanTag->setVID(vlanHeader->getVid());
        vlanTag->setRtag(0xF1C1);
        vlanTag->setRsv(0);
        vlanTag->setSeq(vlanHeader->getVid());
        EV_TRACE << getFullPath() << ": Decapsulating packet and `"
                        << "' passing up contained packet `"
                        << packet->getName() << "' to higher layer" << endl;
        seq = vlanHeader->getVid();

        totalDecap++;
        emit(decapPkSignal, packet->getTreeId());
    } else {
        auto vlanTag = packet->addTagIfAbsent<VLANTagInd>();
        vlanTag->setPcp(kDefaultPCPValue);
        vlanTag->setDe(kDefaultDEIValue);
        vlanTag->setVID(vlanHeader->getVid());
        vlanTag->setRtag(0xF1C1);
        vlanTag->setRsv(0);
        vlanTag->setSeq(vlanHeader->getVid());

    }

    packet->insertAtFront(ethernetMacHeader);
    auto oldFcs = packet->removeAtBack<inet::EthernetFcs>();
    inet::EtherEncap::addFcs(packet, oldFcs->getFcsMode());

    EV_TRACE << " before sending packet up" << endl;
    // Send packet to upper layer
    send(packet, "upperLayerOut");
  }


void BlockDuplicationSwitch::refreshDisplay() const {
    char buf[80];
    sprintf(buf, "up/decap: %ld/%ld\ndown/encap: %ld/%ld", totalFromLowerLayer,
            totalDecap, totalFromHigherLayer, totalEncap);
    getDisplayString().setTagArg("t", 0, buf);
}

void BlockDuplicationSwitch::blockfromupperlayer(inet::Packet *packet, short int seq) {
    pseqh++;
    auto macTag = packet->findTag<MacAddressReq>();
    MACSourceh[pseqh] = macTag->getSrcAddress();

    int snd = 1;



    int nodeID = 0;
    nodeID = getId();

    if (nodeID == 1196 && (seq > 8))
        snd = 2;
    if (nodeID == 489 && (seq > 5))
        snd = 2;
    if (nodeID == 1233 && (seq < 9))
        snd = 2;
    if (nodeID == 249 && (seq % 3 == 0 ))
        snd = 2;

     blockch[pseqh] = seq;

     while (chsum < pseqh)
     {
         if (blockch[chsum] == blockch[pseqh] && MACSourceh[chsum] == MACSourceh[pseqh])
         {
              snd = 2;
         }
         chsum++;

     }
     if (snd == 1)
         send(packet, "lowerLayerOut");
     else
      transmissionSelectionModule->requestPacket();

     }
}

int BlockDuplicationSwitch::getPseql() {
    return pseql;
}
int BlockDuplicationSwitch::getPseqh() {
    return pseqh;
}
int BlockDuplicationSwitch::getPVID() {
    return pvid;
}

} // namespace nesting
