/*
 * BlockDuplication.cc
 * this function work as indiviual recovery function
 *  Created on: Mar 31, 2020
 *      Author: omer
 */




#include "BlockDuplication.h"
#include "inet/common/packet/Packet.h"
#define COMPILETIME_LOGLEVEL omnetpp::LOGLEVEL_TRACE
#include "../../linklayer/ethernet/VLANEncap.h"

namespace nesting {

Define_Module(BlockDuplication);

void BlockDuplication::initialize(int stage) {
   if (stage == inet::INITSTAGE_LOCAL) {
        // Signals
        encapPkSignal = registerSignal("encapPk");
        decapPkSignal = registerSignal("decapPk");

        verbose = par("verbose");
        tagUntaggedFrames = par("tagUntaggedFrames");
        pvid = par("pvid");
        pseq = par("pseq");
        peq = par("peq");

        totalFromHigherLayer = 0;
        WATCH(totalFromHigherLayer);

        totalFromLowerLayer = 0;
        WATCH(totalFromLowerLayer);



        totalEncap = 0;
        WATCH(totalEncap);

        totalDecap = 0;
        WATCH(totalDecap);
    } else if (stage == inet::INITSTAGE_LINK_LAYER) {

    }
}


void BlockDuplication::handleMessage( cMessage* msg) {
    if (dynamic_cast<inet::Request *>(msg) != nullptr) {
        send(msg, "upperLayerOut");
        return;
    }
        peq++;
        if (packet->arrivedOn("lowerLayerIn")) {
              processPacketFromLowerLevel(packet);  //blockage of messages from single ingress port
          } else {
              send(msg, "upperLayerOut");
          }


}


int BlockDuplication::processPacketFromLowerLevel(inet::Packet *packet) {


    peq++;
        auto macTag = packet->findTag<MacAddressReq>();
        MACSourcel[peq] = macTag->getSrcAddress();

         blockch[peq] = seq;
         int chsum = 1;
         int snd = 1;

         while (chsum < pseql)
         {
             if (blockch[chsum] == blockch[pseql] && MACSourcel[chsum] == MACSourcel[pseql])
             {

                     snd = 2;

             }
             chsum++;

         }

         if (snd == 1)
             send(packet, "upperLayerOut");

return blk;
}



void BlockDuplication::refreshDisplay() const {
    char buf[80];
    sprintf(buf, "up/decap: %ld/%ld\ndown/encap: %ld/%ld", totalFromLowerLayer,
            totalDecap, totalFromHigherLayer, totalEncap);
    getDisplayString().setTagArg("t", 0, buf);
}
int BlockDuplication::getPeq() {
    return peq;
}
int BlockDuplication::getPVID() {
    return pvid;
}

}
