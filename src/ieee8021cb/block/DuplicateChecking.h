//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//

#ifndef NESTING_IEEE8021Q_BRIDGE_VLANENCAP_VLANENCAP_H_
#define NESTING_IEEE8021Q_BRIDGE_VLANENCAP_VLANENCAP_H_

#include <omnetpp.h>
#include <tuple>
#include <unordered_map>

#include "inet/linklayer/common/Ieee802Ctrl.h"
#include "../Ieee8021cbHeader_m.h"
#include "inet/linklayer/common/MacAddressTag_m.h"
#include "inet/linklayer/ethernet/EtherFrame_m.h"
#include "../Ether/EthercbFrame_m.h"
#include "inet/linklayer/ethernet/EtherEncap.h"
//#include "../../ieee8021cb/EthercbEncap.h"
#include "inet/common/packet/Packet.h"
#include "inet/common/packet/Message_m.h"
#include "../../linklayer/common/VLANTag_m.h"
#include "nesting/ieee8021q/Ieee8021q.h"
#include "../../common/queue/EtherTrafGenQueue.h"
#include "inet/linklayer/ethernet/EtherMacFullDuplex.h"
#include "inet/linklayer/ethernet/EtherEncap.h"
#include "inet/linklayer/ethernet/EtherPhyFrame_m.h"
#include "nesting/ieee8021q/relay/FilteringDatabase.h"
#include "../../linklayer/ethernet/VLANEncap.h"

#include "inet/linklayer/common/MacAddress.h"
#include "nesting/ieee8021q/clock/IClockListener.h"
#include "../../linklayer/framePreemption/EtherMACFullDuplexPreemptable.h"

#include "inet/common/INETDefs.h"
#include "inet/common/queue/IPassiveQueue.h"
#include "inet/linklayer/ethernet/EtherMacBase.h"
#include "../../ieee8021q/queue/TransmissionSelection.h"
#include "../../ieee8021q/queue/gating/TransmissionGate.h"


using namespace inet;
using namespace omnetpp;

namespace nesting {

class TransmissionSelection;
//class EtherMACFullDuplexPreemptable;
/**
 * See the NED file for a detailed description
 */
class DuplicateChecking:  public cSimpleModule  {
private:
    TransmissionSelection* transmissionSelectionModule;
    /** detailed prints for testing */
    bool verbose;
    /** Parameter from NED file. */
    bool tagUntaggedFrames;
 //   simtime_t cycle = simTime().parse("100us");
    EtherMACFullDuplexPreemptable* ethermacduplex;
    /** Parameter from NED file. */
    int pvid;
    int pseql;
    int pseqh;
    int blockseq[1000];
    int blockch[1000];
    MacAddress MACSourceh[1000];
    MacAddress MACSourcel[1000];

private:
    /**
     * Processes packets from lower level and possibly performs decapsulation
     * when the packet is of type Ether1QTag (is a VLAN Tag)
     * @param packet The packet that was received from lower level.
     */
 //   virtual void processPacketFromLowerLevel(inet::Packet *packet);
    virtual void blockfromupperlayer(inet::Packet *packet,short int seq);
    virtual void blockfromlowerlayer(inet::Packet *packet,short int seq);

    /**
     * Processes packets from higher level and possibly performs
     * encapsulation when the control information says so.
     * @param packet The packet received from  higher level.
     */
 //   virtual void processPacketFromHigherLevel(inet::Packet *packet);
protected:
    /** Signal for encapsulation events. */
    simsignal_t encapPkSignal;

    /** Signal for decapsulation events. */
    simsignal_t decapPkSignal;

    /** Total amount of packets received from higher layer. */
    long totalFromHigherLayer;

    /** Total amount of packets received from lower layer. */
    long totalFromLowerLayer;

    /** Total amount of packets encapsulated. */
    long totalEncap;

    /** Total amount of packets decapsulated. */
    long totalDecap;

    std::vector<TransmissionGate*> tGates;

       /**
        * This vector keeps references to listeners that are notified about
        * packet-enqueued-events. In the default case this should be the Mac
        * module.
        */

       /**
        * This flag is set if a packet is requested from this module by the
        * requestPacket method and again set to false when a packet is send out
        * on the out-gate.
        */
       bool packetRequestedFromUs = false;

       /**
        * This flag is set if a packet is requested from one of the input modules
        * (TransmissionGates) and again set to false if a packet is received on
        * the in-gate.
        */
       bool packetRequestedFromInputs = false;

       /**
        * Set a lower scheduling priority for self-messages than the default
        * value of zero. It is important, that internal events when concurrently
        * scheduled with other events within the queuing network are handled in
        * the end, after all events and successor events that eventually trigger
        * a packetEnqueued-method call are handled.
        */
       int selfMessageSchedulingPriority = 1;

       /**
        * This message is used as a self-message to trigger a request-packet-
        * event after a packet is requested from this module.
        */
       cMessage requestPacketMsg = cMessage("requestPacket");

       /**
        * This message is used as a self-message to trigger packet-enqueued-events
        * due to a packet being enqueued in one of the input modules.
        */
       cMessage packetEnqueuedMsg = cMessage("packetEnqueued");


protected:
    static simsignal_t preemptCurrentFrameSignal;
      static simsignal_t transmittedExpressFrameSignal;
      static simsignal_t startTransmissionExpressFrameSignal;
      static simsignal_t transmittedPreemptableFrameSignal;
      static simsignal_t transmittedPreemptableFramePartSignal;
      static simsignal_t transmittedPreemptableNonFinalSignal;
      static simsignal_t transmittedPreemptableFinalSignal;
      static simsignal_t transmittedPreemptableFullSignal;
      static simsignal_t expressFrameEnqueuedWhileSendingPreemptableSignal;
      static simsignal_t eMacDelay;
      static simsignal_t pMacDelay;
      static simsignal_t receivedExpressFrame;
      static simsignal_t receivedPreemptableFrameFull;
    /** @see cSimpleModule::initialize() */
    virtual void initialize(int stage) override;

    /** @see cSimpleModule::handleMessage(cMessage*) */
    virtual void handleMessage(cMessage *msg) override;
//    virtual void switchmode(int priority) ;


    virtual void processPacketFromLowerLevel(inet::Packet *packet);

    /**
     * Processes packets from higher level and possibly performs
     * encapsulation when the control information says so.
     * @param packet The packet received from  higher level.
     */
    virtual void processPacketFromHigherLevel(inet::Packet *packet);


    /** @see cSimpleModule::refreshDisplay() */
    virtual void refreshDisplay() const override;

    virtual int numInitStages() const override {
        return inet::INITSTAGE_LINK_LAYER + 1;
    }
public:
    virtual ~DuplicateChecking() {
    }
    ;

    virtual int getPVID();
    virtual int getPseql();
    virtual int getPseqh();
};

} // namespace nesting

#endif /* NESTING_IEEE8021Q_BRIDGE_VLANENCAP_VLANENCAP_H_ */
