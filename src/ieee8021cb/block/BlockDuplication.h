/*
 * BlockDuplication.h
 *
 *  Created on: Mar 31, 2020
 *      Author: omer
 */

#ifndef NESTING_IEEE8021CB_BLOCKDUPLICATION_H_
#define NESTING_IEEE8021CB_BLOCKDUPLICATION_H_
#include <omnetpp.h>

#include "inet/linklayer/common/Ieee802Ctrl.h"
#include "../Ieee8021cbHeader_m.h"
#include "inet/linklayer/common/MacAddressTag_m.h"
#include "inet/linklayer/ethernet/EtherFrame_m.h"
#include "../Ether/EthercbFrame_m.h"
#include "inet/linklayer/ethernet/EtherEncap.h"
//#include "EthercbEncap.h"
#include "inet/common/packet/Packet.h"
#include "inet/common/packet/Message_m.h"
#include "../../linklayer/common/VLANTag_m.h"
#include "nesting/ieee8021q/Ieee8021q.h"
#include "../../linklayer/ethernet/VLANEncap.h"

using namespace omnetpp;

namespace nesting {

/**
 * See the NED file for a detailed description
 */
class BlockDuplication: public cSimpleModule {
private:

    /** detailed prints for testing */
    bool verbose;
    /** Parameter from NED file. */
    bool tagUntaggedFrames;

    /** Parameter from NED file. */
    int pvid;
    int peq;
    int blockch[1000];
     MacAddress MACSourceh[1000];
     MacAddress MACSourcel[1000];

private:

    /**
     * Processes packets from lower level and possibly performs decapsulation
     * when the packet is of type Ether1QTag (is a VLAN Tag)
     * @param packet The packet that was received from lower level.
     */
 //   virtual int processPacketFromLowerLevel(inet::Packet *packet);
    virtual int processPacketFromLowerLevel(inet::Packet *packet);

    /**
     * Processes packets from higher level and possibly performs
     * encapsulation when the control information says so.
     * @param packet The packet received from  higher level.
     */
  //  virtual void processPacketFromHigherLevel(inet::Packet *packet);
    virtual void processPacketFromHigherLevel(inet::Packet *packet);
protected:
    /** Signal for encapsulation events. */
    simsignal_t encapPkSignal;

    /** Signal for decapsulation events. */
    simsignal_t decapPkSignal;

    /** Total amount of packets received from higher layer. */
    long totalFromHigherLayer;

    /** Total amount of packets received from lower layer. */
    long totalFromLowerLayer;

    /** Total amount of packets encapsulated. */
    long totalEncap;

    /** Total amount of packets decapsulated. */
    long totalDecap;
protected:
int ch;
    /** @see cSimpleModule::initialize() */
    virtual void initialize(int stage) override;
    virtual int processPacketFromLowerLevel(inet::Packet *packet);
    /** @see cSimpleModule::handleMessage(cMessage*) */
    virtual void handleMessage( cMessage *msg) override;

    /** @see cSimpleModule::refreshDisplay() */
    virtual void refreshDisplay() const override;

    virtual int numInitStages() const override {
        return inet::INITSTAGE_LINK_LAYER + 1;
    }
public:
    virtual ~BlockDuplication() {
    }
    ;

    virtual int getPVID();
    virtual int getPeq();
};

} // namespace nesting





#endif /* NESTING_IEEE8021CB_BLOCKDUPLICATION_H_ */
