/*
 * EthercbEncap.cc
 *
 *  Created on: Apr 13, 2020
 *      Author: omer
 */



/*
 * Copyright (C) 2003 Andras Varga; CTIE, Monash University, Australia
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#include "inet/applications/common/SocketTag_m.h"
#include "inet/common/INETUtils.h"
#include "inet/common/IProtocolRegistrationListener.h"
#include "inet/common/ModuleAccess.h"
#include "inet/common/ProtocolTag_m.h"
#include "inet/common/checksum/EthernetCRC.h"
#include "inet/linklayer/common/FcsMode_m.h"
#include "inet/linklayer/common/Ieee802Ctrl.h"
#include "inet/linklayer/common/Ieee802SapTag_m.h"
#include "inet/linklayer/common/InterfaceTag_m.h"
#include "inet/linklayer/common/MacAddressTag_m.h"
//#include "inet/linklayer/ethernet/EtherEncap.h"
#include "EthercbEncap.h"
//#include "inet/linklayer/ethernet/EtherFrame_m.h"
#include "EthercbFrame_m.h"
#include "inet/linklayer/ethernet/EthernetCommand_m.h"
#include "inet/linklayer/ethernet/EtherPhyFrame_m.h"
#include "inet/linklayer/ieee8022/Ieee8022LlcHeader_m.h"
#include "inet/linklayer/vlan/VlanTag_m.h"
#include "inet/networklayer/contract/IInterfaceTable.h"

namespace nesting {

Define_Module(EthercbEncap);

inet::simsignal_t EthercbEncap::encapPkSignal = registerSignal("encapPk");
inet::simsignal_t EthercbEncap::decapPkSignal = registerSignal("decapPk");
inet::simsignal_t EthercbEncap::pauseSentSignal = registerSignal("pauseSent");

bool EthercbEncap::Socket::matches(inet::Packet *packet, const inet::Ptr<const EthernetcbMacHeader>& ethernetMacHeader)
{
    if (!sourceAddress.isUnspecified() && !ethernetMacHeader->getSrc().isBroadcast() && ethernetMacHeader->getSrc() != sourceAddress)
        return false;
    if (!destinationAddress.isUnspecified() && !ethernetMacHeader->getDest().isBroadcast() && ethernetMacHeader->getDest() != destinationAddress)
        return false;
    if (protocol != nullptr && packet->getTag<inet::PacketProtocolTag>()->getProtocol() != protocol)
        return false;
    if (vlanId != -1 && packet->getTag<inet::VlanInd>()->getVlanId() != vlanId)
        return false;
    return true;
}

void EthercbEncap::initialize(int stage)
{
    Ieee8022Llc::initialize(stage);
    if (stage == inet::INITSTAGE_LOCAL) {
        fcsMode = inet::parseFcsMode(par("fcsMode"));
        seqNum = 0;
        WATCH(seqNum);
        totalFromHigherLayer = totalFromMAC = totalPauseSent = 0;
        useSNAP = par("useSNAP");
        WATCH(totalFromHigherLayer);
        WATCH(totalFromMAC);
        WATCH(totalPauseSent);
    }
    else if (stage == inet::INITSTAGE_LINK_LAYER)
    {
        if (par("registerProtocol").boolValue()) {    //FIXME //KUDGE should redesign place of EtherEncap and LLC modules
            //register service and protocol
            registerService(inet::Protocol::ethernetMac, gate("upperLayerIn"), nullptr);
            registerProtocol(inet::Protocol::ethernetMac, nullptr, gate("upperLayerOut"));
        }
    }
}

void EthercbEncap::processCommandFromHigherLayer(inet::Request *msg)
{
    auto ctrl = msg->getControlInfo();
    if (dynamic_cast<inet::Ieee802PauseCommand *>(ctrl) != nullptr)
        handleSendPause(msg);
    else if (auto bindCommand = dynamic_cast<inet::EthernetBindCommand *>(ctrl)) {
        int socketId = inet::check_and_cast<inet::Request *>(msg)->getTag<inet::SocketReq>()->getSocketId();
        Socket *socket = new Socket(socketId);
        socket->sourceAddress = bindCommand->getSourceAddress();
        socket->destinationAddress = bindCommand->getDestinationAddress();
        socket->protocol = bindCommand->getProtocol();
        socket->vlanId = bindCommand->getVlanId();
        socketIdToSocketMap[socketId] = socket;
        delete msg;
    }
    else if (dynamic_cast<inet::EthernetCloseCommand *>(ctrl) != nullptr) {
        int socketId = inet::check_and_cast<inet::Request *>(msg)->getTag<inet::SocketReq>()->getSocketId();
        auto it = socketIdToSocketMap.find(socketId);
        delete it->second;
        socketIdToSocketMap.erase(it);
        delete msg;
        auto indication = new inet::Indication("closed", inet::ETHERNET_I_SOCKET_CLOSED);
        auto ctrl = new inet::EthernetSocketClosedIndication();
        indication->setControlInfo(ctrl);
        indication->addTagIfAbsent<inet::SocketInd>()->setSocketId(socketId);
        send(indication, "transportOut");
    }
    else if (dynamic_cast<inet::EthernetDestroyCommand *>(ctrl) != nullptr) {
        int socketId = inet::check_and_cast<inet::Request *>(msg)->getTag<inet::SocketReq>()->getSocketId();
        auto it = socketIdToSocketMap.find(socketId);
        delete it->second;
        socketIdToSocketMap.erase(it);
        delete msg;
    }
    else
        Ieee8022Llc::processCommandFromHigherLayer(msg);
}

void EthercbEncap::refreshDisplay() const
{
    Ieee8022Llc::refreshDisplay();
    char buf[80];
    sprintf(buf, "passed up: %ld\nsent: %ld", totalFromMAC, totalFromHigherLayer);
    getDisplayString().setTagArg("t", 0, buf);
}

void EthercbEncap::processPacketFromHigherLayer(inet::Packet *packet)
{
    delete packet->removeTagIfPresent<inet::DispatchProtocolReq>();
    if (packet->getDataLength() > inet::MAX_ETHERNET_DATA_BYTES)
        throw inet::cRuntimeError("packet from higher layer (%d bytes) exceeds maximum Ethernet payload length (%d)", (int)packet->getByteLength(), inet::MAX_ETHERNET_DATA_BYTES);

    totalFromHigherLayer++;
    emit(encapPkSignal, packet);

    // Creates MAC header information and encapsulates received higher layer data
    // with this information and transmits resultant frame to lower layer

    // create Ethernet frame, fill it in from Ieee802Ctrl and encapsulate msg in it
    EV_DETAIL << "Encapsulating higher layer packet `" << packet->getName() << "' for MAC\n";

    int typeOrLength = -1;
    if (!useSNAP) {
        auto protocolTag = packet->findTag<inet::PacketProtocolTag>();
        if (protocolTag) {
            const inet::Protocol *protocol = protocolTag->getProtocol();
            if (protocol) {
                int ethType = inet::ProtocolGroup::ethertype.findProtocolNumber(protocol);
                if (ethType != -1)
                    typeOrLength = ethType;
            }
        }
    }
    if (typeOrLength == -1) {
        Ieee8022Llc::encapsulate(packet);
        typeOrLength = packet->getByteLength();
    }
    auto macAddressReq = packet->getTag<inet::MacAddressReq>();
    const auto& ethHeader = inet::makeShared<EthernetcbMacHeader>();
    ethHeader->setSrc(macAddressReq->getSrcAddress());    // if blank, will be filled in by MAC
    ethHeader->setDest(macAddressReq->getDestAddress());
    ethHeader->setTypeOrLength(typeOrLength);
    packet->insertAtFront(ethHeader);

    EthercbEncap::addPaddingAndFcs(packet, fcsMode);

    packet->addTagIfAbsent<inet::PacketProtocolTag>()->setProtocol(&inet::Protocol::ethernetMac);
    EV_INFO << "Sending " << packet << " to lower layer.\n";
    send(packet, "lowerLayerOut");
}

void EthercbEncap::addPaddingAndFcs(inet::Packet *packet, inet::FcsMode fcsMode, inet::B requiredMinBytes)
{
    inet::B paddingLength = requiredMinBytes - inet::ETHER_FCS_BYTES - inet::B(packet->getByteLength());
    if (paddingLength > inet::B(0)) {
        const auto& ethPadding = inet::makeShared<inet::EthernetPadding>();
        ethPadding->setChunkLength(paddingLength);
        packet->insertAtBack(ethPadding);
    }
    addFcs(packet, fcsMode);
}

void EthercbEncap::addFcs(inet::Packet *packet, inet::FcsMode fcsMode)
{
    const auto& ethFcs = inet::makeShared<inet::EthernetFcs>();
    ethFcs->setFcsMode(fcsMode);

    // calculate Fcs if needed
    if (fcsMode == inet::FCS_COMPUTED) {
        auto ethBytes = packet->peekDataAsBytes();
        auto bufferLength = inet::B(ethBytes->getChunkLength()).get();
        auto buffer = new uint8_t[bufferLength];
        // 1. fill in the data
        ethBytes->copyToBuffer(buffer, bufferLength);
        // 2. compute the FCS
        auto computedFcs = inet::ethernetCRC(buffer, bufferLength);
        delete [] buffer;
        ethFcs->setFcs(computedFcs);
    }

    packet->insertAtBack(ethFcs);
}

const inet::Ptr<const EthernetcbMacHeader> EthercbEncap::decapsulateMacHeader(inet::Packet *packet)
{
    auto ethHeader = packet->popAtFront<EthernetcbMacHeader>();
    packet->popAtBack<inet::EthernetFcs>(inet::ETHER_FCS_BYTES);

    // add Ieee802Ctrl to packet
    auto macAddressInd = packet->addTagIfAbsent<inet::MacAddressInd>();
    macAddressInd->setSrcAddress(ethHeader->getSrc());
    macAddressInd->setDestAddress(ethHeader->getDest());

    // remove Padding if possible
    if (isIeee8023Header(*ethHeader)) {
        inet::b payloadLength = inet::B(ethHeader->getTypeOrLength());
        if (packet->getDataLength() < payloadLength)
            throw inet::cRuntimeError("incorrect payload length in ethernet frame");
        packet->setBackOffset(packet->getFrontOffset() + payloadLength);
        packet->addTagIfAbsent<inet::PacketProtocolTag>()->setProtocol(&inet::Protocol::ieee8022);
    }
    else if (isEth2Header(*ethHeader)) {
        packet->addTagIfAbsent<inet::PacketProtocolTag>()->setProtocol(inet::ProtocolGroup::ethertype.getProtocol(ethHeader->getTypeOrLength()));
    }
    return ethHeader;
}

void EthercbEncap::processPacketFromMac(inet::Packet *packet)
{
    const inet::Protocol *payloadProtocol = nullptr;
    auto ethHeader = decapsulateMacHeader(packet);

    // remove llc header if possible
    if (isIeee8023Header(*ethHeader)) {
        Ieee8022Llc::processPacketFromMac(packet);
        return;
    }
    else if (isEth2Header(*ethHeader)) {
        payloadProtocol = inet::ProtocolGroup::ethertype.getProtocol(ethHeader->getTypeOrLength());
        packet->addTagIfAbsent<inet::PacketProtocolTag>()->setProtocol(payloadProtocol);
        packet->addTagIfAbsent<inet::DispatchProtocolReq>()->setProtocol(payloadProtocol);

        bool stealPacket = false;
        for (auto it : socketIdToSocketMap) {
            auto socket = it.second;
            if (socket->matches(packet, ethHeader)) {
                auto packetCopy = packet->dup();
                packetCopy->setKind(inet::ETHERNET_I_DATA);
                packetCopy->addTagIfAbsent<inet::SocketInd>()->setSocketId(it.first);
                send(packetCopy, "upperLayerOut");
                stealPacket |= socket->vlanId != -1;
            }
        }
        // TODO: should the socket configure if it steals packets or not?
        if (stealPacket)
            delete packet;
        else if (upperProtocols.find(payloadProtocol) != upperProtocols.end()) {
            EV_DETAIL << "Decapsulating frame `" << packet->getName() << "', passing up contained packet `"
                      << packet->getName() << "' to higher layer\n";

            totalFromMAC++;
            emit(decapPkSignal, packet);

            // pass up to higher layers.
            EV_INFO << "Sending " << packet << " to upper layer.\n";
            send(packet, "upperLayerOut");
        }
        else {
            EV_WARN << "Unknown protocol, dropping packet\n";
            inet::PacketDropDetails details;
            details.setReason(inet::NO_PROTOCOL_FOUND);
            emit(inet::packetDroppedSignal, packet, &details);
            delete packet;
        }
    }
    else
        throw inet::cRuntimeError("Unknown ethernet header");

}

void EthercbEncap::handleSendPause(inet::cMessage *msg)
{
    inet::Ieee802PauseCommand *etherctrl = dynamic_cast<inet::Ieee802PauseCommand *>(msg->getControlInfo());
    if (!etherctrl)
        throw inet::cRuntimeError("PAUSE command `%s' from higher layer received without Ieee802PauseCommand controlinfo", msg->getName());
    inet::MacAddress dest = etherctrl->getDestinationAddress();
    int pauseUnits = etherctrl->getPauseUnits();
    delete msg;

    EV_DETAIL << "Creating and sending PAUSE frame, with duration = " << pauseUnits << " units\n";

    // create Ethernet frame
    char framename[40];
    sprintf(framename, "pause-%d-%d", getId(), seqNum++);
    auto packet = new inet::Packet(framename);
    const auto& frame = inet::makeShared<inet::EthernetPauseFrame>();
    const auto& hdr = inet::makeShared<EthernetcbMacHeader>();
    frame->setPauseTime(pauseUnits);
    if (dest.isUnspecified())
        dest = inet::MacAddress::MULTICAST_PAUSE_ADDRESS;
    hdr->setDest(dest);
    packet->insertAtFront(frame);
    hdr->setTypeOrLength(inet::ETHERTYPE_FLOW_CONTROL);
    packet->insertAtFront(hdr);
    EthercbEncap::addPaddingAndFcs(packet, fcsMode);
    packet->addTagIfAbsent<inet::PacketProtocolTag>()->setProtocol(&inet::Protocol::ethernetMac);

    EV_INFO << "Sending " << frame << " to lower layer.\n";
    send(packet, "lowerLayerOut");

    emit(pauseSentSignal, pauseUnits);
    totalPauseSent++;
}

} // namespace inet


