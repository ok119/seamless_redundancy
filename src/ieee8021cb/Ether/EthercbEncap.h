/*
 * EthercbEncap.h
 *
 *  Created on: Apr 13, 2020
 *      Author: omer
 */

#ifndef NESTING_IEEE8021CB_ETHER_ETHERCBENCAP_H_
#define NESTING_IEEE8021CB_ETHER_ETHERCBENCAP_H_


#include "inet/common/packet/Packet.h"
#include "inet/linklayer/common/FcsMode_m.h"
//#include "inet/linklayer/ethernet/EtherFrame_m.h"
#include "EthercbFrame_m.h"
#include "inet/linklayer/ethernet/Ethernet.h"
#include "inet/linklayer/ieee8022/Ieee8022Llc.h"

namespace nesting {

/**
 * Performs Ethernet II encapsulation/decapsulation. More info in the NED file.
 */
class INET_API EthercbEncap : public inet::Ieee8022Llc
{
  protected:
    inet::FcsMode fcsMode = inet::FCS_MODE_UNDEFINED;
    int seqNum;

    // statistics
    long totalFromHigherLayer;    // total number of packets received from higher layer
    long totalFromMAC;    // total number of frames received from MAC
    long totalPauseSent;    // total number of PAUSE frames sent
    static inet::simsignal_t encapPkSignal;
    static inet::simsignal_t decapPkSignal;
    static inet::simsignal_t pauseSentSignal;
    bool useSNAP;    // true: generate EtherFrameWithSNAP, false: generate EthernetIIFrame

    struct Socket
    {
      int socketId = -1;
      inet::MacAddress sourceAddress;
      inet::MacAddress destinationAddress;
      const inet::Protocol *protocol = nullptr;
      int vlanId = -1;

      Socket(int socketId) : socketId(socketId) {}

      bool matches(inet::Packet *packet, const inet::Ptr<const EthernetcbMacHeader>& ethernetMacHeader);
    };

    std::map<int, Socket *> socketIdToSocketMap;

  protected:
    virtual int numInitStages() const override { return inet::NUM_INIT_STAGES; }
    virtual void initialize(int stage) override;

    virtual void processCommandFromHigherLayer(inet::Request *msg) override;
    virtual void processPacketFromHigherLayer(inet::Packet *msg) override;
    virtual void processPacketFromMac(inet::Packet *packet) override;
    virtual void handleSendPause(inet::cMessage *msg);

    virtual void refreshDisplay() const override;

  public:
    static void addPaddingAndFcs(inet::Packet *packet, inet::FcsMode fcsMode, inet::B requiredMinByteLength = inet::MIN_ETHERNET_FRAME_BYTES);
    static void addFcs(inet::Packet *packet, inet::FcsMode fcsMode);

    static const inet::Ptr<const EthernetcbMacHeader> decapsulateMacHeader(inet::Packet *packet);
};

} // namespace inet




#endif /* NESTING_IEEE8021CB_ETHER_ETHERCBENCAP_H_ */
